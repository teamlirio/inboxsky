package com.xurpas.inbox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvEmailList;
    private FrameLayout frame_selectContianer;
    private Button btnSelect, btnDeselect;
    private EmailAdapter emailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frame_selectContianer = findViewById(R.id.framelo_selectall);
        btnDeselect = findViewById(R.id.button_deselectall);
        btnSelect = findViewById(R.id.button_selectall);
        rvEmailList = findViewById(R.id.recycler_main);
        rvEmailList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        emailAdapter = new EmailAdapter(MainActivity.this, filler());
        rvEmailList.setAdapter(emailAdapter);
    }

    private ArrayList<UserEmail> filler() {
        ArrayList<UserEmail> list = new ArrayList<>();
        Log.v("MAIN", new Paragraphs().sampleParagraph());
        list.add(new UserEmail("Title 1", "Dec 17, 2017", "11:12am", new Paragraphs().sampleParagraph()));
        list.add(new UserEmail("Title 2", "Dec 18, 2017", "12:12am", new Paragraphs().sampleParagraph()));
        list.add(new UserEmail("Title 3", "Dec 19, 2017", "01:17pm", new Paragraphs().sampleParagraph()));
        list.add(new UserEmail("Title 4", "Jan 17, 2018", "05:30pm", new Paragraphs().sampleParagraph()));
        list.add(new UserEmail("Title 5", "Mar 17, 2018", "10:21pm", new Paragraphs().sampleParagraph()));
        list.add(new UserEmail("Title 6", "Feb 17, 2018", "11:44pm", new Paragraphs().sampleParagraph()));

        return list;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.inbox_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete :
                if(frame_selectContianer.getVisibility() == View.VISIBLE) {
                    frame_selectContianer.setVisibility(View.GONE);
                    item.setTitle("DELETE");

                } else {
                    frame_selectContianer.setVisibility(View.VISIBLE);

                    item.setTitle("CANCEL");
                }

        }
        return true;
    }
}
