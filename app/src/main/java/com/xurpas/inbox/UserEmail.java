package com.xurpas.inbox;

/**
 * Created by fluxion inc on 27/03/2018.
 */

public class UserEmail {
    private String title;
    private String date;
    private String time;
    private String message;

    public UserEmail() {
    }

    public UserEmail(String title, String date, String time, String message) {
        this.title = title;
        this.date = date;
        this.time = time;
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
