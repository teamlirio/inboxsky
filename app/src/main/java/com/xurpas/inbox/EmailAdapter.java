package com.xurpas.inbox;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by fluxion inc on 27/03/2018.
 */

public class EmailAdapter extends RecyclerView.Adapter<EmailAdapter.EmailAdapterViewHolder> {
    private Context mContext;
    private ArrayList<UserEmail> userEmailList;
    private View rootView;
    private boolean checkBoxes;


    public EmailAdapter (Context context, ArrayList<UserEmail> mailList) {
        mContext = context;
        userEmailList = mailList;
    }

    @Override
    public EmailAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.email_inbox, parent, false);
        return new EmailAdapterViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(EmailAdapterViewHolder holder, int position) {
        holder.textTitle.setText(userEmailList.get(position).getTitle());
        holder.textMessage.setText(userEmailList.get(position).getMessage().substring(0, 30));
        holder.textDateTime.setText(userEmailList.get(position).getDate() + " | " + userEmailList.get(position).getTime());

    }
    @Override
    public int getItemCount() {
        return userEmailList.size();
    }

    public class EmailAdapterViewHolder extends RecyclerView.ViewHolder {

        private TextView textTitle,textMessage, textDateTime;
       // public CheckBox cbEmailSelect;

        public EmailAdapterViewHolder(View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_emailinbox_title);
            textMessage = itemView.findViewById(R.id.text_emailinbox_message);
            textDateTime = itemView.findViewById(R.id.text_emailinbox_datetime);
           // cbEmailSelect = itemView.findViewById(R.id.checkbox_emailinbox);
        }
    }
}
